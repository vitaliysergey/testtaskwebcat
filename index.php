<?php
$json = json_encode([
    'pid' => "yicllz",
    'offerId' => 62 ,
    'city' => "",
    // other required fields of the response
    'firstname'=>"Vitaly",
    'lastname'=>"Sergey",
    'email'=>'vitaly@test.com',
    'phone'=>'1234567',
    'ref'=>'',
    'ip'=>'192.168.1.1',
    'country'=>'BY',
]);
$ch = curl_init('https://admin.neogara.com/api/lid');
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json),
]);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
$response = curl_exec($ch);
curl_close($ch);
var_dump($response);